include("miniep8.jl")

using Test
function testaEP()
	println(" ")
	println("Testando funcoes...")
	println(" ") 
	print("compareByValue... ")
	@test compareByValue("2♠", "A♠") == true
	@test compareByValue("K♡", "10♡") == false
    @test compareByValue("10♠", "10♡") == false
    @test compareByValue("2♡", "A♠") == true
    @test compareByValue("7♠","6♡") == false
	println("OK!")

	print("insercao...       ")
    #@test insercao(["10♡", "10♢", "K♠", "A♠", "J♠", "A♠"]) == ["10♡", "10♢", "J♠", "K♠", "A♠", "A♠"] # para compareByValue
    #@test insercao(["9♡", "2♢", "J♠","A♠","7♠", "J♠", "A♠"]) == ["2♢", "7♠", "9♡", "J♠", "J♠", "A♠", "A♠"]
    @test insercao(["10♡", "10♢", "K♠", "A♠", "J♠", "A♠"]) == ["10♢", "J♠", "K♠", "A♠", "A♠","10♡"] # para compareByValueAndSuit
    @test insercao(["9♡", "2♢", "J♠","A♠","7♠", "J♠", "A♣"]) == ["2♢", "7♠", "J♠", "J♠", "A♠","9♡","A♣"]
    #Salvar arquivo em miniep8 antes de testar com os novos valores!
    println("OK!")
    
    print("compareByValueAndSuit... ")
    @test compareByValueAndSuit("2♠", "A♠") == true
	@test compareByValueAndSuit("K♡", "10♡") == false
	@test compareByValueAndSuit("10♠", "10♡") == true
    @test compareByValueAndSuit("A♠","2♡") == true
    @test compareByValueAndSuit("2♡", "A♠") == false
    println("OK!")
	println(" ")
	printstyled("Todos os testes obtiveram sucesso!", bold = true, color = :green)
end
testaEP()
