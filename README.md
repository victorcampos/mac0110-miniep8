# MAC0110-MiniEP8

MiniEP8 coursework for MAC0110 discipline.

In this MiniEP8 we'll be using sorting algorithms, starting
with an insertion sort program for Julia given in class but 
adapted to use strings representing card values and its suits.

The card values are in the order 2 < 3 < ... < 10 < J < Q < K < A.

In exercise 2.1, the compareByValue(x,y) function will receive
the values of two "playing cards", represented by strings composed
of a value and a suit - e.g. 'A♠' for an ace of spades - and return 
a boolean 'true' if the card in x has a lesser value than the card y, 
and a boolean 'false' if vice-versa or if they're equal.

In exercise 2.2, the compareByValueAndSuit(x,y) function will act 
similarly, but this time the card's suit will also be taken into 
account - mind that the suit precedes the card's value itself. 
The order of the cards' suits are presented as follows:
♦ < ♠ < ♥ < ♣.  

USAGE: julia testMiniEP8.jl # Runs pre-set automated tests, which
can be modified at the end user's will