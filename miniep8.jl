function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])	#if v[j] < v[j - 1] | vamos usar a compareByValue ou
				troca(v, j, j - 1)				# compareByValueAndSuit pra verse o elemento em j-1 é menor
			else
				break
			end
			j = j - 1
		end
	end
return v
end

function compareByValue(x,y) # true caso x < y 
	valores = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"] # usar 'findfirst(isequal(X),valores)'
	#vamos dividir x e y em caracteres e dar valores pra cada um
	#na primeira função, não precisamos analisar o naipe
	caracteresDeX = split(x,"")
	caracteresDeY = split(y,"")
	cartaX = caracteresDeX[1]
	cartaY = caracteresDeY[1]
	if cartaX == "1"  # pelo nosso método, podemos ter problemas com a carta 10
		cartaX = "10"	# como não temos carta "1", podemos aplicar uma solução fácil
	end
	if cartaY == "1"
		cartaY = "10"
	end
	valorx = findfirst(isequal(cartaX), valores)
	valory = findfirst(isequal(cartaY), valores)
	if valorx < valory
		return true
	else
		return false
	end
end

function compareByValueAndSuit(x,y)
	valores = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"] # usar 'findfirst(isequal(X),valores)'
	naipes = ["♢","♠","♡","♣"] # comparamos primeiro o naipe, dps o valores
	caracteresDeX = split(x,"")
	caracteresDeY = split(y,"")
	cartaX = caracteresDeX[1]
	cartaY = caracteresDeY[1]
	naipeX = caracteresDeX[2]
	naipeY = caracteresDeY[2]
	if cartaX == "1"  
		cartaX = "10"
		naipeX = caracteresDeX[3]	
	end
	if cartaY == "1"
		cartaY = "10"
		naipeY = caracteresDeY[3]
	end
	valorNaipeX = findfirst(isequal(naipeX), naipes)
	valorNaipeY = findfirst(isequal(naipeY), naipes)
	if valorNaipeX < valorNaipeY # Se o naipe de X for pior ou ao de Y
		return true
	elseif valorNaipeX == valorNaipeY # se o naipe de X for igual ao de Y
		compareByValue(x,y)
	else # se o naipe de X for maior que o de Y
		return false
	end
end

